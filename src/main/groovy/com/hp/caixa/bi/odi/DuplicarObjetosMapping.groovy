/**
  * Script para copiar componentes dentro de una asignación (mapping).
  * TO-DO: Implementar la copia de propiedades para todos los tipos de componente.
  *
  * @author jesusvg
  * @version 0.4
  **/

import groovy.swing.SwingBuilder;

import java.awt.FlowLayout;
import java.security.MessageDigest;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;

import oracle.odi.core.persistence.transaction.ITransactionStatus;
import oracle.odi.core.persistence.transaction.support.DefaultTransactionDefinition;
import oracle.odi.domain.mapping.component.AggregateComponent;
import oracle.odi.domain.mapping.component.DistinctComponent;
import oracle.odi.domain.mapping.component.ExpressionComponent;
import oracle.odi.domain.mapping.component.FilterComponent;
import oracle.odi.domain.mapping.component.JoinComponent
import oracle.odi.domain.mapping.component.LookupComponent;
import oracle.odi.domain.mapping.component.SetComponent;
import oracle.odi.domain.mapping.component.SorterComponent;
import oracle.odi.domain.mapping.component.SplitterComponent
import oracle.odi.domain.mapping.exception.MapComponentException;
import oracle.odi.domain.mapping.finder.IMapComponentTypeFinder;
import oracle.odi.domain.mapping.finder.IMappingFinder;
import oracle.odi.domain.mapping.InputConnectorPoint;
import oracle.odi.domain.mapping.MapComponent
import oracle.odi.domain.mapping.MapComponentDelegator;
import oracle.odi.domain.mapping.MapComponentOwner;
import oracle.odi.domain.mapping.MapConnectorPoint;
import oracle.odi.domain.mapping.OutputConnectorPoint
import oracle.odi.domain.project.finder.IOdiFolderFinder;
import oracle.odi.domain.project.finder.IOdiProjectFinder;

// Método para crear un componente de la misma clase que otro componente.

def duplicarComponente (def componente) {
  claseComponente = Class.forName(componente.class.getName(), true, this.class.classLoader)
  constructorComponente = claseComponente.getConstructor(MapComponentOwner.class, String.class)
  copiaComponente = constructorComponente.newInstance(componente.getComponentOwner(), componente.getName())
  
  return copiaComponente
}

// Método para calcular el número de antecedentes de primer nivel de un componente.

def numeroAntecedentes (def componente) {
  numeroAntecedentes = 0
  puntosEntrada = componente.getInputConnectorPoints()
  
  if (puntosEntrada != null && puntosEntrada.size() > 0) {
    for (p in puntosEntrada) {
      conectores = p.getFromConnectors()
      
      if (conectores != null && conectores.size() > 0) {
        for (c in conectores) {
          puntoSalidaAnterior = c.getStartPoint()
          
          if (puntoSalidaAnterior != null) {
            componenteAnterior = puntoSalidaAnterior.getOwningComponent()
            
            if (componenteAnterior != null) {
              numeroAntecedentes += 1
            }
          }
        }
      }
    }
  }
  
  return numeroAntecedentes
}

// Método para obtener los antecedentes de primer nivel de un componente.

def obtenerAntecedentes (def componente) {
  if (numeroAntecedentes(componente) > 0) {
    antecedentes = new ArrayList<Object>()
    puntosEntrada = componente.getInputConnectorPoints()
    
    if (puntosEntrada != null && puntosEntrada.size() > 0) {
      for (p in puntosEntrada) {
        conectores = p.getFromConnectors()
        
        if (conectores != null && conectores.size() > 0) {
          for (c in conectores) {
            puntoSalidaAnterior = c.getStartPoint()
            
            if (puntoSalidaAnterior != null) {
              componenteAnterior = puntoSalidaAnterior.getOwningComponent()
              
              if (componenteAnterior != null) {
                antecedentes.add(componenteAnterior)
              }
            }
          }
        }
      }
    }
    
    return antecedentes
  } else {
    return null
  }
}

// Método para obtener todos los componentes antecedentes a un componente.

def obtenerTodosAntecedentes (def componente, def todosAntecedentes, def incluir) {
  if (numeroAntecedentes(componente) > 0) {
    antecedentes = obtenerAntecedentes(componente)
    
    if (antecedentes != null && antecedentes.size() > 0) {
      for (a in antecedentes) {
        obtenerTodosAntecedentes(a, todosAntecedentes, true)
      }
    }
  }
  
  if (incluir) {
    todosAntecedentes.add(componente)
  }
}

// Método para obtener los nombres de todos los componentes antecedentes a un componente.

def obtenerNombreTodosAntecedentes (def componente) {
  nombres = new ArrayList<String>()
  todosAntecedentes = new ArrayList<Object>()
  
  obtenerTodosAntecedentes(componente, todosAntecedentes, false)
  
  if (todosAntecedentes.size() > 0) {
    for (ant in todosAntecedentes) {
      nombres.add(ant.getName())
    }
    
    return nombres
  } else {
    return null
  }
}

// Método para sustituir las referencias en las expresiones de un componente.

def sustituirReferenciasExp (def origen, def destino) {
  tipoComponente = origen.class.getName().tokenize('.')[-1]
  
  switch (tipoComponente) {
    case "ExpressionComponent":
      todosAntecedentes = new ArrayList<Object>()
      obtenerTodosAntecedentes(origen, todosAntecedentes, false)
      
      if (todosAntecedentes.size() > 0) {
        sustituciones = new HashMap<String, String>()
        
        for (ant in todosAntecedentes) {
          copiaTmp = new ExpressionComponent(origen.getComponentOwner(), ant.getName())
          sustituciones.put(ant.getName(), copiaTmp.getName())
          copiaTmp.removeComponent(copiaTmp, false)
        }
        
        if (sustituciones.size() > 0) {
          for (atributo in destino.getAttributes()) {
            expresion = atributo.getExpression().getText()
            newExpresion = new String(expresion)
            
            for (s in sustituciones) {
              newExpresion = newExpresion.replaceAll(s.getKey(), s.getValue())
            }
            
            atributo.setExpressionText(newExpresion)
          }
        }
      }
      
      break
    
    default:
      println("AVISO: Sustitución de referencias no soportada para el componente " +
              "de tipo " + tipoComponente + ".")
      break
  }
}

// Método para copiar las propiedades de un componente a otro de la misma clase.

def copiarComponente (def origen, def destino) {
  tipoOrigen = origen.class.getName().tokenize('.')[-1]
  tipoDestino = destino.class.getName().tokenize('.')[-1]
  
  if (tipoOrigen != null && tipoDestino != null && tipoOrigen.equals(tipoDestino)) {
    switch (tipoOrigen) {
      case "AggregateComponent":
        origen = (AggregateComponent)origen
        destino = (AggregateComponent)destino
        
        for (atributo in origen.getAttributes()) {
          atrDst = destino.addAttribute(atributo.getName(),
                                        atributo.getExpression().getText(),
                                        atributo.getDataType(),
                                        atributo.getSize(),
                                        atributo.getScale())
          
          atrDst.setDescription(atributo.getDescription())
          atrDst.setExecuteOnHint(atributo.getExecuteOnHint())
          atrDst.setExecuteOnLocation(atributo.getExecuteOnLocation())
          atrDst.setGroupFunction(atributo.isGroupFunction())
        }
        
        destino.setDescription(origen.getDescription())
        destino.setHavingText(origen.getHavingText())
        destino.setManualGroupBy(origen.getManualGroupBy().getText())
        destino.setExecuteOnHint(origen.getExecuteOnHint())
        
        break
      
      case "LookupComponent":
        origen = (LookupComponent)origen
        destino = (LookupComponent)destino
        
        destino.setJoinConditionText((origen.getJoinConditionText()))
        destino.setExecuteOnHint(origen.getExecuteOnHint())
        
        destino.setJoinType(origen.getJoinType())
        destino.setNoMatchRows(origen.getNoMatchRows())
        destino.setNthRowNumber(origen.getNthRowNumber())
        
        destino.setDescription(origen.getDescription())
        destino.setActive(origen.isActive())
        
        break
      
      case "SetComponent":
        origen = (SetComponent)origen
        destino = (SetComponent)destino
        
        // Se renombran los puntos de entrada creados por defecto.
        
        md = MessageDigest.getInstance("MD5")
        
        for (p in destino.getInputConnectorPoints()) {
          md.update(p.getName().getBytes("UTF-8"))
          p.setName(new String(md.digest()))
        }
        
        // Se copian los puntos de entrada del objeto de origen.
        
        puntosEntradaOrg = origen.getInputConnectorPoints()
        puntosOrigenDst = new HashMap<String, InputConnectorPoint>()
        puntosOrigenDstArray = new ArrayList<MapConnectorPoint>()
        tmpCompArray = new ArrayList<SetComponent>()
        
        for (p in puntosEntradaOrg) {
          pDest = destino.createInputConnectorPoint(p.getName())
          
          pDest.setDescription(p.getDescription())
          
          try {
            destino.setSetOperationType(pDest, origen.getSetOperationType(p))
          } catch (Exception exc) {}
          
          puntosOrigenDst.put(p.getName(), pDest)
          puntosOrigenDstArray.add(pDest)
          
          tmpComp = new SetComponent(origen.getComponentOwner(), "TMP_SET")
          tmpComp.getOutputConnectorPoints().get(0).connectTo(pDest)
          tmpCompArray.add(tmpComp)
        }
        
        for (t in tmpCompArray) {
          t.removeComponent(t, false)
        }
        
        // Se copian los atributos y sus expresiones.
        
        for (atributo in origen.getAttributes()) {
          expList = atributo.getExpressions()
          expArray = new String[puntosOrigenDstArray.size()]
          numExp = 0
          
          for (i = 0; i < puntosOrigenDstArray.size(); i++) {
            newExp = new String("")
            
            try {
              for (e in expList) {
                if (e.getScopingInputPoint().getName().equals(puntosOrigenDstArray.get(i).getName())) {
                  newExp = new String(e.getExpressionText())
                  numExp += 1
                }
              }
            } catch (Exception exc) {
              newExp = new String("")
            }
            
            expArray[i] = newExp
          }
          
          atrDst = destino.addSetAttribute(atributo.getName(), expArray)
          
          atrDst.setDescription(atributo.getDescription())
          atrDst.setDataType(atributo.getDataType())
          atrDst.setSize(atributo.getSize())
          atrDst.setScale(atributo.getScale())
          atrDst.setExecuteOnLocation(atributo.getExecuteOnLocation())
          
          expDst = atrDst.getExpressions()
          
          for (expOrg in atributo.getExpressions()) {
            for (expDst in atrDst.getExpressions()) {
              if (expOrg.getScopingInputPoint().getName().equals(expDst.getScopingInputPoint().getName())) {
                expDst.setText(expOrg.getText())
                expDst.setExecuteOnHint(expOrg.getExecuteOnHint())
                expDst.setExecuteOnLocation(expOrg.getExecuteOnLocation())
              }
            }
          }
          
          if (numExp == 1) {
            atrDst.setExecuteOnLocation(atributo.getExecuteOnLocation())
          }
        }
        
        // Se copia la configuración general del componente.
        
        destino.setDescription(origen.getDescription())
        destino.setExecuteOnHint(origen.getExecuteOnHint())
        
        break
      
      case "DistinctComponent":
        origen = (DistinctComponent)origen
        destino = (DistinctComponent)destino
        
        for (atributo in origen.getAttributes()) {
          atrDst = destino.addAttribute(atributo.getName(),
                                        atributo.getExpression().getText(),
                                        atributo.getDataType(),
                                        atributo.getSize(),
                                        atributo.getScale())
          
          atrDst.setExecuteOnHint(atributo.getExecuteOnHint())
          atrDst.setExecuteOnLocation(atributo.getExecuteOnLocation())
          atrDst.setDescription(atributo.getDescription())
        }
        
        destino.setDescription(origen.getDescription())
        destino.setExecuteOnHint(origen.getExecuteOnHint())
        
        break
      
      case "SplitterComponent":
        origen = (SplitterComponent)origen
        destino = (SplitterComponent)destino
        
        // Se copia la configuración general del componente.
        
        destino.setDescription(origen.getDescription())
        destino.setExecuteOnHint(origen.getExecuteOnHint())
        
        // Se guardan los puntos de salida creados por defecto.
        
        puntosSalidaDest = destino.getOutputConnectorPoints()
        
        // Se renombran los puntos de entrada creados por defecto.
        
        md = MessageDigest.getInstance("MD5")
        
        for (p in puntosSalidaDest) {
          md.update(p.getName().getBytes("UTF-8"))
          p.setName(new String(md.digest()))
        }
        
        // Se copian los puntos de salida del objeto de origen.
        
        puntosSalidaOrg = origen.getOutputConnectorPoints()
        tmpCompArray = new ArrayList<SetComponent>()
        
        for (p in puntosSalidaOrg) {
          pDest = destino.createOutputConnectorPoint(p.getName())
          
          pDest.setDescription(p.getDescription())
          destino.setSplitterCondition(pDest, origen.getSplitterCondition(p).getText())
          destino.setRemainder(pDest, origen.isRemainder(p))
          
          tmpComp = new SplitterComponent(origen.getComponentOwner(), "TMP_SPLIT")
          pDest.connectTo(tmpComp.getInputConnectorPoints().get(0))
          tmpCompArray.add(tmpComp)
        }
        
        for (t in tmpCompArray) {
          t.removeComponent(t, false)
        }
        
        // Y se eliminan los puntos de salida creados por defecto.
        
        for (p in puntosSalidaDest) {
          destino.removeOutputConnectorPoint(p)
        }
        
        break
      
      case "ExpressionComponent":
        origen = (ExpressionComponent)origen
        destino = (ExpressionComponent)destino
        
        for (atributo in origen.getAttributes()) {
          atrDst = destino.addExpression(atributo.getName(),
                                         atributo.getExpression().getText(),
                                         atributo.getDataType(),
                                         atributo.getSize(),
                                         atributo.getScale())
          
          atrDst.setExecuteOnHint(atributo.getExecuteOnHint())
          atrDst.setExecuteOnLocation(atributo.getExecuteOnLocation())
          atrDst.setDescription(atributo.getDescription())
        }
        
        destino.setDescription(origen.getDescription())
        destino.setExecuteOnHint(origen.getExecuteOnHint())
        
        break
      
      case "FilterComponent":
        origen = (FilterComponent)origen
        destino = (FilterComponent)destino
        
        destino.setFilterCondition(origen.getFilterConditionText())
        destino.setExecuteOnHint(origen.getExecuteOnHint())
        destino.setDescription(origen.getDescription())
        destino.setActive(origen.isActive())
        
        break
      
      case "SorterComponent":
        origen = (SorterComponent)origen
        destino = (SorterComponent)destino
        
        destino.setSorterCondition(origen.getSorterConditionText())
        destino.setExecuteOnHint(origen.getExecuteOnHint())
        destino.setDescription(origen.getDescription())
        
        break
      
      case "JoinComponent":
        origen = (JoinComponent)origen
        destino = (JoinComponent)destino
        
        // Se copian las condiciones de la unión.
        
        destino.setJoinConditionText(origen.getJoinConditionText())
        destino.setJoinType(origen.getJoinType())
        destino.setGenerateANSISyntax(origen.isGenerateANSISyntax())
        destino.setJoinOrder(origen.getJoinOrder())
        destino.setExecuteOnHint(origen.getExecuteOnHint())
        
        // Se copia la configuración general del componente.
        
        destino.setDescription(origen.getDescription())
        destino.setActive(origen.isActive())
        
        // Se renombran los puntos de entrada creados por defecto.
        
        md = MessageDigest.getInstance("MD5")
        
        for (p in destino.getInputConnectorPoints()) {
          md.update(p.getName().getBytes("UTF-8"))
          p.setName(new String(md.digest()))
        }
        
        // Se copian los puntos de entrada del objeto de origen.
        
        puntosEntradaOrg = origen.getInputConnectorPoints()
        tmpCompArray = new ArrayList<SetComponent>()
        
        for (p in puntosEntradaOrg) {
          pDest = destino.createInputConnectorPoint(p.getName())
          
          pDest.setDescription(p.getDescription())
          
          tmpComp = new JoinComponent(origen.getComponentOwner(), "TMP_JOIN")
          tmpComp.getOutputConnectorPoints().get(0).connectTo(pDest)
          tmpCompArray.add(tmpComp)
        }
        
        for (t in tmpCompArray) {
          t.removeComponent(t, false)
        }
        
        break
      
      default:
        println("AVISO: Copia de atributos no soportada para el componente " +
                "de tipo " + tipoOrigen + ".")
        break
    }
  } else {
    println("AVISO: No es posible copiar las propiedades entre dos componentes de distinto tipo.")
  }
}

// Método para generar el cuadro de diálogo y capturar la selección.

def ventanaCopiar(pf, cf, af) {
  proyectos = []
  carpetas = []
  asignaciones = []
  componentes = []
  
  listaProyectos = []
  listaCarpetas = []
  listaAsignaciones = []
  listaComponentes = []
  
  // Se carga la lista de proyectos.
  
  proyectos = pf.findAll()
  
  for (proyecto in proyectos) {
    listaProyectos.add(proyecto.getCode())
  }
  
  listaProyectos.sort()
  listaProyectos.addAll(0, '—')
  
  // Se define el manejador de la interfaz y algunas propiedades.
  
  def gui = new SwingBuilder()
  gui.setVariable('cuadroDialogo-properties', [:])
  dimension = new java.awt.Dimension(205, 20)
  def variables = gui.variables
  
  // Se definen los combos del cuadro de diálogo.
  
  def proyectoCombo
  def carpetaCombo
  def asignacionCombo
  def componenteCombo
  def subExpresionCheck
  
  // Se define el cuadro de diálogo.
  
  def cuadroDialogo = gui.dialog(title: 'Copia de componentes', id: 'cuadroDialogo', modal: true) {
    panel() {
      boxLayout(axis: BoxLayout.Y_AXIS)
      
      panel(alignmentX: 0f) {
        flowLayout(alignment: FlowLayout.CENTER)
        label('Script para la copia de componentes de asignaciones.')
      }
      
      // Selector de proyectos:
      
      panel(alignmentX: 0f) {
        flowLayout(alignment: FlowLayout.RIGHT)
        label('Proyecto:')
        proyectoCombo = gui.comboBox(id: 'proyectoCombo',
                                     items: listaProyectos,
                                     preferredSize: dimension,
                                     itemStateChanged: {event ->
                                       carpetaCombo.removeAllItems()
                                       asignacionCombo.removeAllItems()
                                       componenteCombo.removeAllItems()
                                       
                                       carpetaCombo.addItem('—')
                                       asignacionCombo.addItem('—')
                                       componenteCombo.addItem('—')
                                       
                                       carpetaCombo.setEnabled(false)
                                       asignacionCombo.setEnabled(false)
                                       componenteCombo.setEnabled(false)
                                       subExpresionCheck.setEnabled(false)
                                       botonCopiar.setEnabled(false)
                                       
                                       if (proyectoCombo.selectedIndex > 0) {
                                         carpetas = cf.findByProject(proyectoCombo.selectedItem)
                                         
                                         if (carpetas != null && carpetas.size() > 0) {
                                           listaCarpetas = []
                                           
                                           for (carpeta in carpetas) {
                                             listaCarpetas.add(carpeta.getName())
                                           }
                                           
                                           listaCarpetas.sort()
                                           
                                           for (carp in listaCarpetas) {
                                             carpetaCombo.addItem(carp)
                                           }
                                           
                                           carpetaCombo.setEnabled(true)
                                         }
                                       }
                                     })
      }
      
      // Selector de carpetas:
      
      panel(alignmentX: 0f) {
        flowLayout(alignment: FlowLayout.RIGHT)
        label('Carpeta:')
        carpetaCombo = gui.comboBox(id: 'carpetaCombo',
                                    items: ['—'],
                                    enabled: false,
                                    preferredSize: dimension,
                                    itemStateChanged: {event ->
                                      asignacionCombo.removeAllItems()
                                      componenteCombo.removeAllItems()
                                      
                                      asignacionCombo.addItem('—')
                                      componenteCombo.addItem('—')
                                      
                                      asignacionCombo.setEnabled(false)
                                      componenteCombo.setEnabled(false)
                                      subExpresionCheck.setEnabled(false)
                                      botonCopiar.setEnabled(false)
                                      
                                      if (carpetaCombo.selectedIndex > 0) {
                                        asignaciones = af.findByProject(proyectoCombo.selectedItem,
                                                                        carpetaCombo.selectedItem)
                                        
                                        if (asignaciones != null && asignaciones.size() > 0) {
                                          listaAsignaciones = []
                                          
                                          for (asignacion in asignaciones) {
                                            listaAsignaciones.add(asignacion.getName())
                                          }
                                          
                                          listaAsignaciones.sort()
                                          
                                          for (asig in listaAsignaciones) {
                                            asignacionCombo.addItem(asig)
                                          }
                                          
                                          asignacionCombo.setEnabled(true)
                                        }
                                      }
                                    })
      }
      
      // Selector de asignaciones (mappings):
      
      panel(alignmentX: 0f) {
        flowLayout(alignment: FlowLayout.RIGHT)
        label('Asignación:')
        asignacionCombo = gui.comboBox(id: 'asignacionCombo',
                                       items: ['—'],
                                       enabled: false,
                                       preferredSize: dimension,
                                       itemStateChanged: {event ->
                                         componenteCombo.removeAllItems()
                                         
                                         componenteCombo.addItem('—')
                                         
                                         componenteCombo.setEnabled(false)
                                         subExpresionCheck.setEnabled(false)
                                         botonCopiar.setEnabled(false)
                                         
                                         if (asignacionCombo.selectedIndex > 0) {
                                           componentes = (af.findByName(asignacionCombo.selectedItem,
                                                                        proyectoCombo.selectedItem,
                                                                        carpetaCombo.selectedItem))[0].getAllComponents()
                                           
                                           if (componentes != null && componentes.size() > 0) {
                                             listaComponentes = []
                                             
                                             for (componente in componentes) {
                                               listaComponentes.add(componente.getName())
                                             }
                                             
                                             listaComponentes.sort()
                                             
                                             for (comp in listaComponentes) {
                                               componenteCombo.addItem(comp)
                                             }
                                             
                                             componenteCombo.setEnabled(true)
                                           }
                                         }
                                       })
      }
      
      // Selector de componentes:
      
      panel(alignmentX: 0f) {
        flowLayout(alignment: FlowLayout.RIGHT)
        label('Componente:')
        componenteCombo = gui.comboBox(id: 'componenteCombo',
                                       items: ['—'],
                                       enabled: false,
                                       preferredSize: dimension,
                                       itemStateChanged: {event ->
                                         if (componenteCombo.selectedIndex > 0) {
                                           botonCopiar.setEnabled(true)
                                           
                                           asignacion = (af.findByName(asignacionCombo.selectedItem,
                                                                       proyectoCombo.selectedItem,
                                                                       carpetaCombo.selectedItem))[0]
                                           
                                           componente = asignacion.findComponent(componenteCombo.selectedItem)
                                           
                                           if ((componente.class.getName().tokenize('.')[-1]).equals("ExpressionComponent")) {
                                             subExpresionCheck.setEnabled(true)
                                           } else {
                                             subExpresionCheck.setEnabled(false)
                                           }
                                         } else {
                                           botonCopiar.setEnabled(false)
                                         }
                                       })
      }
      
      // Casilla de sustitución de referencias en los ExpressionComponent:
      
      panel(alignmentX: 0f) {
        flowLayout(alignment: FlowLayout.RIGHT)
        subExpresionCheck = gui.checkBox(id: 'subExpresionCheck',
                                         enabled: false,
                                         selected: true,
                                         text: "Sustituir expresión")
      }
      
      // Casilla de copia de antecedentes:
      
      panel(alignmentX: 0f) {
        flowLayout(alignment: FlowLayout.RIGHT)
        copyAntecedentesCheck = gui.checkBox(id: 'copyAntecedentesCheck',
                                             enabled: true,
                                             selected: true,
                                             text: "Copiar antecedentes")
      }
      
      // Panel con los botones:
      
      panel(alignmentX:0f) {
        flowLayout(alignment: FlowLayout.CENTER)
        
        // Botón que copia el componente seleccionado:
        
        botonCopiar = gui.button('Copiar',
                                 preferredSize: [80, 24],
                                 enabled: false,
                                 actionPerformed: {
                                   if (componenteCombo.selectedIndex > 0) {
                                     // Es necesario el TransactionManager y el estado para poder persistir los cambios.
                                     
                                     tDef = new DefaultTransactionDefinition()
                                     tm = odiInstance.getTransactionManager()
                                     tme = odiInstance.getTransactionalEntityManager()
                                     tStatus = tm.getTransaction(tDef)
                                     
                                     // Se obtiene la referencia al componente a copiar.
                                     
                                     asignacion = (af.findByName(asignacionCombo.selectedItem,
                                                                 proyectoCombo.selectedItem,
                                                                 carpetaCombo.selectedItem))[0]
                                     
                                     componente = asignacion.findComponent(componenteCombo.selectedItem)
                                     
                                     // Se copia el componente y se añade a la asignación.
                                     
                                     copiaComponente = duplicarComponente(componente)
                                     copiarComponente(componente, copiaComponente)
                                     
                                     if (subExpresionCheck.isEnabled() && subExpresionCheck.isSelected()) {
                                       // Se sustituyen las referencias en la expresión.
                                       
                                       sustituirReferenciasExp(componente, copiaComponente)
                                     }
                                     
                                     asignacion.addComponent(copiaComponente)
                                     
                                     // Se persisten los cambios.
                                     
                                     tm.commit(tStatus)
                                     
                                     // Y se recarga el combo de componentes.
                                     
                                     componenteCombo.removeAllItems()
                                     componenteCombo.addItem('—')
                                     
                                     componentes = (af.findByName(asignacionCombo.selectedItem,
                                                                  proyectoCombo.selectedItem,
                                                                  carpetaCombo.selectedItem))[0].getAllComponents()
                                     
                                     if (componentes != null && componentes.size() > 0) {
                                       listaComponentes = []
                                       
                                       for (componente in componentes) {
                                         listaComponentes.add(componente.getName())
                                       }
                                       
                                       listaComponentes.sort()
                                       
                                       for (comp in listaComponentes) {
                                         componenteCombo.addItem(comp)
                                       }
                                     }
                                     
                                     subExpresionCheck.setEnabled(false)
                                     botonCopiar.setEnabled(false)
                                   }
                                 })
        
        // Botón para salir:
        
        botonCerrar = gui.button('Cerrar',
                                 preferredSize: [80, 24],
                                 actionPerformed: {
                                   variables.dialogResult = 'Cerrar'
                                   dispose()
                                 })
      }
    }
  }
  
  cuadroDialogo.pack()
  cuadroDialogo.show()
  
  return variables
}

// CÓDIGO PRINCIPAL:

// Se crean los finders.

proyectosFinder = (IOdiProjectFinder)odiInstance.getTransactionalEntityManager().getFinder(OdiProject.class)
carpetasFinder = (IOdiFolderFinder)odiInstance.getTransactionalEntityManager().getFinder(OdiFolder.class);
asignacionesFinder = (IMappingFinder)odiInstance.getTransactionalEntityManager().getFinder(Mapping.class);

// Se abre la ventana para copiar componentes.

ventanaCopiar(proyectosFinder, carpetasFinder, asignacionesFinder)